//SHANNON TAYLOR/////////////
//HW03, PROGRAM 1
//DUE SEPTEMBER 18, 2018
//CSE 002-210
//PROGRAM DESCRIPTION: This program gathers user input of acres of land affected by hurricane precipitation
//and the average number of inches of rainfall in those areas. The output will be the amount of rainfall in cubic miles. 

import java.util.Scanner;

public class Convert
{
  
  public static void main(String args[])
  {
    Scanner myScanner = new Scanner(System.in);
    
    ////VARIABLE DECLARATIONS
    double acres;
    double inches;
    double rainfall;
    final double GAL_PER_IN = 27154.2857; //number of gallons/inch of rain. SOURCE: US Geological Survey
    final double MI_PER_GAL = 9.08169E-13 ; //number mi^3/gallon. 
    
    
    ////USER INPUTS
    System.out.print("Enter the affected area in acres: ");
    acres = myScanner.nextDouble();
    System.out.print("Enter the rainfall in affected area (inches): ");
    inches = myScanner.nextDouble();
    
    ////CALCULATIONS
    rainfall = inches * acres; //returns acre inches 
    rainfall *= GAL_PER_IN; //converts acre inches to gallons
    rainfall *= MI_PER_GAL; //converts gallons to cubic miles 
    
    ////PRINT
    System.out.println("There are " + rainfall + " cubic miles of rainfall in " + acres + " acres.");
 
  }
  
}

