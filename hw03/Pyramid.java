//SHANNON TAYLOR/////////////
//HW03, PROGRAM 2
//DUE SEPTEMBER 13, 2018
//CSE 002-210
//PROGRAM DESCRIPTION: This program gathers user input of dimensions of a square pyramid and returns the volume.

import java.util.Scanner;

public class Pyramid
{
  
  public static void main(String args[])
  {
    Scanner myScanner = new Scanner(System.in);
    
    //VARIABLE DECLARATIONS
    double length; //length of one side of square pyramid base 
    double height; //height of square pyramid
    double volume; //volume of the pyramid
    
    //USER INPUT
    System.out.print("Enter the length of the base of a SQUARE pyramid: ");
    length = myScanner.nextDouble();
    System.out.print("Enter the height of a SQUARE pyramid: ");
    height = myScanner.nextDouble();
    
    //CALCULATION
    //VOLUME OF SQUARE PYRAMID = (LENGTH^2 * HEIGHT) / 3
    volume = (length * length * height) / 3; 
    
    //PRINT
    System.out.println("Volume of Square Pyramid: " + volume);
    
    
    
    
  }
  
}


