//SHANNON TAYLOR/////////////
//HW04, PROGRAM 2
//DUE SEPTEMBER 18, 2018
//CSE 002-210
//PROGRAM DESCRIPTION:
import java.util.Scanner;

public class CrapsIf
{
  
  public static void main(String args[])
  {
    Scanner myScan = new Scanner(System.in);
    String userChoice = "";
    int die1 = 0; 
    int die2 = 0;
    int val;
    String slang = "";
    
    System.out.print("Type \"R\" to randomly roll 2 dice. Type \"C\" to provide two dice values:  ");
    userChoice = myScan.nextLine();
    userChoice = userChoice.toUpperCase(); //puts user output in uppercase because .equals is case sensitive
    
    if(userChoice.equals("R"))  //generate 2 random values on each die 1-6 
    {
      die1 = (int)(Math.random() * ((6 - 1) + 1)) + 1; //generates a random number between 1 and 6 -->roll 1
      die2 = (int)(Math.random() * ((6 - 1) + 1)) + 1; //generates a random number between 1 and 6 -->roll 2
    }
    else if (userChoice.equals("C"))  //GET USER INPUT AND ASSIGN THESE INTEGERS
    {
      System.out.print("Enter integer 1-6 (inclusive) for the first roll:  ");
      die1 = myScan.nextInt(); //user choice of first roll 
      System.out.print("Enter integer 1-6 (inclusive) for the second roll:  ");
      die2 = myScan.nextInt(); //user choice of second roll 
    }
       
    val = die1 + die2; //takes the sum of the two dice for evaluation 
   
    //checks sum to properly assign the slang name 
    if (val == 2)
      slang = "Snake Eyes";
    else if(val == 3)
      slang = "Ace Deuce";
    else if(val == 5)
      slang = "Fever Five";
    else if(val == 7)
      slang = "Seven Out";
    else if(val == 9)
      slang = "Nine";
    else if(val == 11)
      slang = "Yo-leven";
    else if(val == 12)
      slang = "Box Cars"; 
    //^^^^^^^^^^^^^^^^^^^^^^^^^^ above only have 1 option (doing them first eliminates running through many nested ifs)
    // vvvvvvvvvvvvvvvvvvvvvvvvv below have multiple slang options 
    else if(val == 4) 
    {
      if(die1 == 2) //e.g. if they sum to 4 and the first is 2, it must be "hard 4"
        slang = "Hard Four";
      else          //e.g. any other value of die 1 that can still sum to be 4 must be "easy 4" 
        slang = "Easy Four";
    }
     else if(val == 6)
    {
       if(die1 == 3)
         slang = "Hard Six";
       else
         slang = "Easy Six";
    }
     else if(val == 8)
    {
       if(die1 == 4)
         slang = "Hard Eight";
       else
         slang = "Easy Eight";
    }

    else if(val == 10)
    {
       if(die1 == 5)
         slang = "Hard Ten";
       else
         slang = "Easy Ten";
    }
    
    System.out.println("Roll 1: " + die1);
    System.out.println("Roll 2: " + die2);
    System.out.println("You rolled a(n) " + slang + "!");
 
  }
}
