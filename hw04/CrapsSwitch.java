//SHANNON TAYLOR/////////////
//HW04, PROGRAM 2
//DUE SEPTEMBER 25, 2018
//CSE 002-210
//PROGRAM DESCRIPTION: **USING SWITCH STATEMENTS** this program allows for random rolls of two 6-sided die, or user selected rolls.
//It evaluates the values of the rolls and prints the appropriate "craps slang name" 

import java.util.Scanner;

public class CrapsSwitch
{
  
  public static void main(String args[])
  {
    Scanner myScan = new Scanner(System.in);
    String userChoice = "";
    int die1 = 0; 
    int die2 = 0;
    int val;
    String slang = "";
    
    System.out.print("Type \"R\" to randomly roll 2 dice. Type \"C\" to provide two dice values:  ");
    userChoice = myScan.nextLine();
    userChoice = userChoice.toUpperCase(); //puts user output in uppercase because .equals is case sensitive
    
    switch(userChoice)
    {
      case "R": //user enters an r , it makes two random numbers for rolls 
          die1 = (int)(Math.random() * ((6 - 1) + 1)) + 1; //generates a random number between 1 and 6 -->roll 1
          die2 = (int)(Math.random() * ((6 - 1) + 1)) + 1; //generates a random number between 1 and 6 -->roll 2
          break;
      case "C": //user enters a c, it lets user enter two numbers for rolls 
          System.out.print("Enter integer 1-6 (inclusive) for the first roll:  ");
          die1 = myScan.nextInt(); //user choice of first roll 
          System.out.print("Enter integer 1-6 (inclusive) for the second roll:  ");
          die2 = myScan.nextInt(); //user choice of second roll 
          break; 
    }
    
    val = die1 + die2; //adds the two numbers to then evaluate 
   
    System.out.println(val);
  
  switch(val) //checks the sum of the two values 
   {
    case 2:
       slang = "Snake Eyes";
       break;
    case 3:
       slang = "Ace Deuce";
       break;
    case 5:
       slang = "Fever Five";
       break;
    case 7:
       slang = "Seven Out";
       break;
    case 9:
       slang = "Nine";
       break;
    case 11:
       slang = "Yo-leven";
       break;
    case 12:
       slang = "Box Cars";
       break;
  //////////////////////////////NESTED BELOW
    case 4:
      switch(die1)
      {
        case 2:
          slang = "Hard Four";
          break;
        default:
          slang = "Easy Four";
          break; 
      }
      break;
      
     case 6:
      switch(die1)
      {
        case 3:
          slang = "Hard Six";
          break;
        default:
          slang = "Easy Six";
          break; 
      }
      break;
      
      case 8:
      switch(die1)
      {
        case 4:
          slang = "Hard Eight";
          break;
        default:
          slang = "Easy Eight";
          break; 
      }
      break;
      
      case 10:
      switch(die1)
      {
        case 5:
          slang = "Hard Ten";
          break;
        default:
          slang = "Easy Ten";
          break; 
      }
      break;
  
  }
    System.out.println("Roll 1: " + die1);
    System.out.println("Roll 2: " + die2);
    System.out.println("You rolled a(n) " + slang + "!");
 
  }
}
