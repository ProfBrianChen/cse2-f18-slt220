//SHANNON TAYLOR/////////////
//DUE: TUESDAY, SEPT 4TH
//CSE 002-210
//PROGRAM DESCRIPTION: prints a personal welcome and autobiographical statement

public class WelcomeClass{
  
  public static void main(String args[]){
    
    System.out.println("-----------"); 
    System.out.println("| WELCOME |"); 
    System.out.println("-----------"); 
    System.out.println("^  ^  ^  ^  ^  ^"); 
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\"); // "backslash" acts as an escape character to print \ 
    System.out.println("<-S--L--T--2--2--0->"); //prints personal ID number
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("v  v  v  v  v  v"); 
    System.out.println("My name is Shannon, I am a junior " +
        "majoring in bioengineering with a business minor. " +
        "In my free time, I enjoy running, cooking, and watching The Office."); //prints personal autobiographical statement 
 
  }
}