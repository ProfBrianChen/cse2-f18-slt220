/*
Shannon Taylor
Homework 6 
This program gets user input from 0 to 100, and outputs a hidden "X" within
a field of astrisk chars (*). The size of the field is determined by the user 
input, which is first validated to be in the correct range, of correct type
*/

import java.util.Scanner;

public class EncryptedX {

 
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        int size;
        
        //VALIDATES INPUT (an integer, between 0 and 100)///////////////////////
        do {    
            System.out.print("Enter an integer between 1 and 100: ");
            
            while(!scan.hasNextInt()) //only enters if input is not type int
            {
                System.out.print("Error, not an integer: "); //error msg
                scan.next(); //clears buffer of erraneous value  
            }
            size = scan.nextInt(); //assigns if correct, will be overwritten if not

        }while(size > 100 || size < 0); //keeps looping to  get 0-100
        
        //PRINTS THE PATTERN///////////////////////////////////////////////////
        //NOTE: this prints a size+1 by size+1 grid, but that matches the 
        ////////example code posted on course site!!! (the middle row has 
        ////////number of astrisks equal to the size inputted)
        
        for(int i = 0; i<=size; i++) //prints # rows, zero indexed
        {
            for(int j = 0; j<=size; j++) //prints # cols, zero indexed
            {
                if(j==i) //spaces "diagonal down" (eg. 2nd letter of 2nd line = space)
                {
                    System.out.print(" "); 
                }
                else if(j==(size-i)) //spaces diagonal up (eg. 2nd line, 2nd to last letter = space)
                {                           
                    System.out.print(" "); 
                }
                else //prints a * everywhere else 
                {
                    System.out.print("*"); 
                }
            }
            System.out.println();
        }
        
       
    }
    
}
