/*
SHANNON TAYLOR
HOMEWORK 5 : TWIST GENERATOR
DESCRIPTION: this program gets a positive integer from the user and verifies it. 
it takes this value and prints a twist of that length, as shown below length=25 
\ /\ /\ /\ /\ /\ /\ /\ /\
 X  X  X  X  X  X  X  X  
/ \/ \/ \/ \/ \/ \/ \/ \/
*/

import java.util.Scanner;

public class TwistGenerator{

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int length; //length that user inputs 
        int count; //"length" of each row will be counted seperately though equal 
        
        
        //ensure a valid input 
        do {           //body executes at least once (to get input) 
            System.out.print("Enter a positive integer: ");//asks for + input
            
            while(!scan.hasNextInt()) //only enters if input is not type int
            {
                System.out.print("Error, not an integer: "); //error msg
                scan.next(); //clears buffer of erraneous value  
            }
            length = scan.nextInt(); //assigns if correct, will be overwritten if not

        }while(length <= 0); //keeps looping to  get a number 1 or bigger
        
        System.out.println("Twist of length " + length + ":");
        
        count = length;
     
        
 
        //ROW NUMBER ONE
        while(count > 0) //while the length has a positive value
        {
            System.out.print("\\"); //prints initial backslash 
            count--; //subtracts length to account for being printed 
            if(count == 0) //checks if it has printed all indicies of length
                break; //breaks out of loop immediately if no more left to count
            System.out.print(" ");
            count--;
              if(count == 0)
                break;
            System.out.print("/");
            count--;
              if(length == 0)
                break; 
              //if the count is still non zero, it will loop again 
        }
        
        System.out.println(); //new line for next row
        count = length; //reinitializes count for the next row
        
        //ROW NUMBER TWO
        while(count > 0) //while the length has a positive value
        {
            System.out.print(" "); //prints initial backslash 
            count--; //subtracts length to account for being printed 
            if(count == 0) //checks if it has printed all indicies of length
                break; //breaks out of loop immediately if no more left to count
            System.out.print("X");
            count--;
              if(count == 0)
                break;
            System.out.print(" ");
            count--;
              if(length == 0)
                break; 
              //if the count is still non zero, it will loop again 
        }
        
        System.out.println(); //new line for next row
        count = length; //reinitializes count for the next row
        
        //ROW NUMBER THREE
        while(count > 0) //while the length has a positive value
        {
            System.out.print("/"); //prints initial backslash 
            count--; //subtracts length to account for being printed 
            if(count == 0) //checks if it has printed all indicies of length
                break; //breaks out of loop immediately if no more left to count
            System.out.print(" ");
            count--;
              if(count == 0)
                break;
            System.out.print("\\");
            count--;
              if(length == 0)
                break; 
              //if the count is still non zero, it will loop again 
        }
        
        System.out.println();
   
    }
}
