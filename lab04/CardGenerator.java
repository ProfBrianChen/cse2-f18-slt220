//SHANNON TAYLOR/////////////
//LAB 04
//CSE 002-210
//PROGRAM DESCRIPTION: this program generates a random card from a deck of 52 cards and presents the value


public class CardGenerator
{
  
  public static void main(String args[])
  {
 
    
    String suit = "";
    String cardVal = "";
    
    int card = (int)(Math.random()*(52 + 1) +1); //generates a random number between 1 and 52 
    int val; 
    
    //DETERMINE CARD SUIT
    if (card >= 1 && card <= 13) //between 1 and 13 is diamonds
      suit = "diamonds ";
    else if (card >= 14 && card <= 26) //between 14 and 26 is clubs
      suit = "clubs ";
    else if (card >= 27 && card <= 39) //between 27 and 39 is hearts
      suit = "hearts ";
    else                              //between 40 and 52 is spades
      suit = "spades ";
    
    //Determine Value of the Card
    val = card % 13; //checks the remainder because the cards are in groups of 13
    switch(val)
    {
      case 1:
        cardVal = "ace "; //remainder of 1 is ace e.g. 1%13 = 1 (ace of diamond)
        break;
      case 0:
        cardVal = "king "; //remainder of 0 is king e.g. 13%13 = 0 (king of diamond)
        break;
      case 12:
        cardVal = "queen "; //remainder of 12 is queen e.g. 12%13 = 12 (queen of diamond)
        break;
      case 11: 
        cardVal = "jack "; //remainder of 11 is jack e.g. 11%13 = 11 (jack of diamond)
        break;
      default: 
        cardVal = "" + val; //any other value of remainder is the actual value of the card
        break; 
    }
    System.out.println(card);
    System.out.println("You drew a " + cardVal + " " + "of " + suit);
      
    
    


  }
  
}