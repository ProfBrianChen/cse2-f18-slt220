///Shannon Taylor, Becca Grady, Kevin Andrefsky 
//Group E 
//

import java.util.Scanner;

public class ReportWhile
{
  
  public static void main(String args[])
  {
    Scanner scan = new Scanner(System.in);
    
    int numTrans = 0;
    double max = 0;
    double currTrans = 0;
    double total = 0;
    double avg = 0;
    int count =1;
    String junk = "";

    
    System.out.print("Enter the number of transactions made: ");
    while(! scan.hasNextInt() ) 
    {
      System.out.print("Error, enter a integer: "); //prints error message 
      junk = scan.next(); //clears the buffer of erroneous value 
    }
    numTrans = scan.nextInt(); 
    
    while(count <= numTrans)
    {
      System.out.print("Enter the value of the " + count + " transaction: "); 
       while(! scan.hasNextDouble() ) 
      {
        System.out.print("Error, enter a double $XX.XX: "); //prints error message 
        junk = scan.next(); //clears the buffer of erroneous value 
      }
      currTrans = scan.nextDouble(); 
     
      if(currTrans > max)
         {
           max = currTrans; 
         }
      
      total += currTrans; //increments the total with each trans that user enters
      count++;
    }
    
    avg = total / (count-1);  //calculates average, subtracts 1 since count is 1 more 
      

     System.out.println("Total: " + total);
     System.out.println("Average: " + avg);
     System.out.println("Maximum Transaction: " + max);
    
    
    
    
    
    
    
    
  }
  
  
}