///Shannon Taylor, Becca Grady, Kevin Andrefsky 
//Group E 
//VemnoVariables
public class VemnoVariables
{
  
  public static void main(String args[])
  {

double intBal = 100; //initial balance of bank account ($100)
double transAmt = 2; //amount transfered from your bank account to another users' 
int bankAcctNum; //user's personal bank account number
double receiveAmt = 5.50; //amount received from another person to your acct
double currBal = 0.0; //current balance of Vemno account

int routingNum; //bank routing number of user
int phoneNum; //user phone number
double transFee; //3% fee on credit card transactions 
String userID = "slt220"; //personal user vemno ID 
int numFriends; //number of friends on Vemno
int password; //personal Vemno password
String notification = "A"; //notifications from receiving or sending money 

currBal = intBal + receiveAmt - transAmt; //transaction of receiving and sending money 


System.out.println("User ID " + userID); //prints user vemno ID 
System.out.println("Initial Account Balance " + intBal);
System.out.println("- Transfer Amount " + transAmt);
System.out.println("+ Received Amount " + receiveAmt);
System.out.print("Current Balance = " +  currBal);      //prints a transfer & received payment transactions & impact on current balance
  }
}
    
    



