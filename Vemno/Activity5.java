/*
Group #: E
Group Members: Shannon Taylor, Becca Grady, Kevin Andrefsky
Program Description: This program should imitate a basic Venmo menu system. Until the user types in a "4", the menu will continue to loop through and process transactions. Each choice (1-4) prompts the computer to complete a different function. For example, if the user types 3, the user's current balance should be printed out.

Before starting to add code, you should walk through the CT planning process. We have included multiple questions to guide the process of decomposition/algorithmic thinking.
*/

import java.util.Scanner;
public class Activity5{
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    
    int choice = 0; //choice will hold the users selection/input
    // input-- what variables will be needed?
    //balance, variable to store menu #, $ requested, $ sent
    
    double balance = 0;
    double rcvd = 0;
    double sent = 0;
    String user = ""; //who they want to send to 
    
    System.out.print("Enter the initial balance of your account.");
    balance = input.nextDouble(); //gets initial bal of account frm user 
    
 
    //So far, the do-while loop simply prints out the menu options. What else needs to happen here?
    //it needs to actually do the calculations , print results
    do{
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      //If the user types 1, what should the program do? Use psuedocode or english words in comments
      //ask them how much they want to send, check if enough $,  to who they want to send it to, update new balance


      
      //If the user types 1, what is the expected output? Use psuedocode or english words in comments
      //you sent $xx to abcd, current balance is yy 
      
      
      System.out.println("2. Request Money");
      //If the user types 2, what should the program do? Use psuedocode or english words in comments
      //ask how much they request, who they request from, purpose of request,  add amount to balance
      
      
      //If the user types 2, what is the expected output? Use psuedocode or english words in comments
      //you received $xx from abcd, current balance is yy 
   
      System.out.println("3. Check Balance");
      //If the user types 3, what should the program do? Use psuedocode or english words in comments
      //print current balance stored
      
 
      //If the user types 3, what is the expected output? Use psuedocode or english words in comments
      //the current balance of acct
      
   
      System.out.println("4. Quit");
      //if the user types 4, what should the program do? Use psuedocode or english words in comments
      //print you left app, exit loop
 
 
      //If the user types 4, what is the expected output? Use psuedocode or english words in comments
      //you have left Vemno (print), wont reprint menu again 
      

      //Implement the program functionality that you defined below:
      choice = input.nextInt(); //user enters choice 1-4
      
      if(choice == 1)
      {
        System.out.print("Who do you want to send to? ");
        user = input.next(); 
        
       do{
         System.out.print("Enter amount to send to " + user + "\n"
                          + "must be less than/equal to " + balance + "): $");
            
            while(!input.hasNextDouble()) //only enters if input is not type double
            {
                System.out.print("Error, enter a valid dollar amount: "); //error msg
                input.next(); //clears buffer of erraneous value  
            }
            sent = input.nextDouble(); //assigns if correct, will be overwritten if not

        }while(sent > balance); //keeps looping until sufficient funds avaliable 
       
        balance = balance - sent; 
        System.out.println("$" + sent + " to " + user);
        System.out.println("New Balance: " + balance);
       
         
       }
        
        
        
        //if(choice == 2)
      
        //getting money 
      
        //if(choice == 3)
      
        //check bal
      
        //if(choice == 4)
      
        //quit
      
      
      
      
      
      
      
      choice = 4; 

    }while(choice != 4);
  }
}
    