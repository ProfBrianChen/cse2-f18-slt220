import java.util.Scanner;

public class Hw10 {

    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int row, col;
        int numMoves = 0, move = 0; //odd player 1, if even player 2 move
        boolean gameWon = false; 
        char [][] board = {
                            {'1','2','3'},
                            {'4','5','6'},
                            {'7','8','9'}
                          };//3x3 board, initial state 
        
        System.out.println("X's go first.");

        do{                         
            displayBoard(board); 
            move = getMove(board); //gets a valid move and checks if taken
            ++numMoves;
            
            row = getRow(move);
            col = getCol(move);
            
            System.out.println("You selected position: " + move);

            if(numMoves % 2 != 0) //odd number of moves ADD X 
                board[row][col] = 'X';

            else if(numMoves % 2 == 0) //even number of moves ADD O
                board[row][col] = 'O';
            
            gameWon = checkWin(board);

        }while(numMoves < 9 && gameWon == false); //board not maxed out, noone won yet
        
        System.out.println("--Final Board--");
        displayBoard(board); //display the final board
        //print out the game result
        if(numMoves % 2 != 0 && gameWon)
            System.out.println("X's Win!"); 
        else if(numMoves % 2 == 0 && gameWon)
            System.out.println("O's Win!");
        else
            System.out.println("It's a draw!");

    }
    
    //////////////////////////////////////////////////////////////////
    /**
     * displayBoard is void; takes char array board as param
     * prints out the current board 
     */
    public static void displayBoard(char [][] board) {
        int row = board.length;
        int col = board[0].length;
        for(int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }
    //////////////////////////////////////////////////////////////////
    /**
     * getMove takes char array board as param
     * ensures: user enters an int, user enters between 1 and 9 
     * user board spot has not been taken 
     * returns the move number integer
     */
    public static int getMove(char[][]board) {
        int move;
        boolean check = false; 
        do{
            System.out.print("Enter a valid move...");
            move = getInt(); //get an integer
            if (move < 1 || move > 9) {
                System.out.println("Out of Range");
            }
            else if(board[getRow(move)][getCol(move)] =='X' || board[getRow(move)][getCol(move)] =='O'){
                System.out.println("Invalid Move, spot occupied."); 
            } 
            else {
                check = true; 
            }
        }while(check == false);
 
        return move;
  
    }
    //////////////////////////////////////////////////////////////////
    /**
     * getInt checks if some input is an integer
     * loops until user enters an int
     */
    public static int getInt() {
        Scanner input = new Scanner(System.in);
        while(input.hasNextInt() == false){
            System.out.println("You entered and invalid value -- try again");
            input.nextLine();//clear buffer
     }
     return input.nextInt();
  } 
    
    //////////////////////////////////////////////////////////////////////////
    /**
     * associates the users move with the corresponding row
     * returns row number integer
     */
    public static int getRow(int move) {
        if(move == 1 || move == 2 || move == 3)
            return 0;
        else if(move == 4 || move == 5 || move == 6)
            return 1;
        else
            return 2;
    }
    //////////////////////////////////////////////////////////////////////////
    public static int getCol(int move) {
    /**
     * associates the users move with the corresponding column
     * returns col number integer
     */
        if(move == 1 || move == 4 || move == 7)
            return 0;
        else if(move == 2 || move == 5 || move == 8)
            return 1;
        else
            return 2;
    }
    //////////////////////////////////////////////////////////////////////////
    /**
     * takes char[][] as param, returns true if one of the eight possible winning
     * conditions are met 
     */
    public static boolean checkWin(char [][] board) {
        return (board[0][0] == board [0][1] && board[0][0] == board [0][2]) || //row zero
               (board[0][0] == board [1][0] && board[0][0] == board [2][0]) || //col zero
               (board[1][0] == board [1][1] && board[1][0] == board [1][2]) || //row one 
               (board[0][1] == board [1][1] && board[0][1] == board [2][1]) || //col one
               (board[2][0] == board [2][1] && board[2][0] == board [2][2]) || //row two
               (board[0][2] == board [1][2] && board[0][2] == board [2][2]) || //col two
               (board[0][0] == board [1][1] && board[0][0] == board [2][2]) || //down diag 
               (board[2][0] == board [1][1] && board[0][0] == board [0][2]);   //up diag
    }
    
  
}
