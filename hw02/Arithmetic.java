//SHANNON TAYLOR/////////////
//HW02, DUE SEPTEMBER 11, 2018
//CSE 002-210
//PROGRAM DESCRIPTION:

public class Arithmetic
{
  
  public static void main(String args[])
  {
    //INPUT VARIABLE DECLARATIONS
    //PANTS (number purchased and unit price)
    int numPants = 3; 
    double pantsPrice = 34.98;
    //SHIRTS (number purchased and unit price)
    int numShirts = 2;
    double shirtsPrice = 24.99;
    //BELTS (number purchased and unit price)
    int numBelts = 1;
    double beltsPrice = 33.99;
    //PENNSYLVANIA SALES TAX
    final double PA_SALES_TAX = 0.06; 
    
    //ARITHMETIC VARIABLE DECLARATIONS
    double pantsOnly, //cost all of pants, NO TAX
    shirtsOnly, //cost all of shirts, NO TAX
    beltsOnly, //cost all of belts, NO TAX
    taxPants, //sales tax on pants
    taxShirts, //sales tax on shirts
    taxBelts, //sales tax on belts
    totCostPants, //cost of pants, WITH TAX
    totCostShirts, //cost of pants, WITH TAX
    totCostBelts, //cost of pants, WITH TAX
    subtotal, //total cost of all items no taxes 
    totTax, //total sales tax on purchase
    totTrans; //total transaction cost, all items with taxes
    
    System.out.println(" ");
    System.out.println("---------Transaction Receipt----------");
   
    //PANTS
    pantsOnly = ( (int)(numPants * pantsPrice * 100) ) / 100.0; //calculates total cost of pants, no tax, 2 decimal places
    taxPants = ( (int)(pantsOnly * PA_SALES_TAX * 100) ) / 100.0; //calculates total sales tax on the pants 
    totCostPants = pantsOnly + taxPants; //calculates the cost of pants with sales tax INCLUDED
    System.out.println("Subtotal: Pants (3)           $" + pantsOnly);
    System.out.println("Sales Tax: Pants                $" + taxPants);
    System.out.println("Total: Pants                  $" + totCostPants);
    System.out.println(" ");
    //SHIRTS
    shirtsOnly = ( (int)(numShirts * shirtsPrice * 100) ) / 100.0; //calculates total cost of pants, no tax, 2 decimal places
    taxShirts = ( (int)(shirtsOnly * PA_SALES_TAX * 100) ) / 100.0; //calculates total sales tax on the pants 
    totCostShirts = shirtsOnly + taxShirts; //calculates total cost of pants with sales tax INCLUDED 
    System.out.println("Subtotal: Sweatshirts (2)     $" + shirtsOnly);
    System.out.println("Sales Tax: Sweatshirts          $" + taxShirts);
    System.out.println("Total: Sweatshirts            $" + totCostShirts);
    System.out.println(" ");
    //BELTS
    beltsOnly = ( (int)(numBelts * beltsPrice * 100) ) / 100.0; //calculates total cost of pants, no tax, 2 decimal places
    taxBelts = ( (int)(beltsOnly * PA_SALES_TAX * 100) ) / 100.0; //calculates total sales tax on the pants 
    totCostBelts = beltsOnly + taxBelts; //calculates total cost of pants with sales tax INCLUDED 
    System.out.println("Subtotal: Belts (1)           $" + beltsOnly);
    System.out.println("Sales Tax: Belts                $" + taxBelts);
    System.out.println("Total: Belts                  $" + totCostBelts);
    //TOTALS
    subtotal = pantsOnly + shirtsOnly + beltsOnly; //calculates total cost of the items without sales tax
    totTax = taxPants + taxShirts + taxBelts; //calcs total tax on entire transaction
    totTrans = totCostPants + totCostShirts + totCostBelts; //total transaction cost WITH TAX INCLUDED, ALL ITEMS
    System.out.println("---------------------------------------");
    System.out.println("Subtotal                     $" + subtotal);
    System.out.println("Total Tax (PA Sales Tax)       $" + totTax);
    System.out.println("Transaction Total            $" + totTrans);
    System.out.println("---------------------------------------");
     System.out.println(" ");
    
  }
  
} 