/*
SHANNON TAYLOR
LAB 6 : PYRAMIDS
PATTERN A
DESCRIPTION: Asks for user input between 1 and 10 (length of the pyramid ie number of rows)
**checks that input is integer between 1 and 10 
outer loop determines how many lines will be printed.
inner loop determines what will be printed on each line (prints until it reaches value of the outer loop )
prints pattern a
*/

import java.util.Scanner;

public class PatternA{

    public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      
      int length; 
  
       //GETTING USER INPUT/////////////////////////
       do {           //body executes at least once (to get input) 
            System.out.print("Enter a number between 1 and 10: ");
            
            while(!scan.hasNextInt()) //only enters if input is not type int
            {
                System.out.println("Error, not an integer. Try Again."); 
                System.out.print("Enter a number between 1 and 10: ");
              
                scan.next(); //clears buffer of erraneous value  
            }
           length = scan.nextInt(); //assigns if correct, will be overwritten if not

        }while(length > 10 || length < 1); //keeps looping until number is in the range 
      
      
      for(int numRows = 1; numRows <= length; numRows++)
      {
        for (int numCol = 1; numCol <= numRows; ++numCol)
        {
          System.out.print(numCol + " ");
        }
        System.out.println();
      }
      
      
      

 

      
      
    }
}
