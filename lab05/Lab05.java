//SHANNON TAYLOR/////////////
//LAB05
//CSE 002-210
//PROGRAM DESCRIPTION: This program takes input from the user concerning course information and ensures that it is of the proper type.
//if it is incorrect, it reprompts the user to enter the information again. At the end, it reprints all of the information. 

import java.util.Scanner;

public class Lab05
{
  
  public static void main(String args[])
  {
    Scanner scan = new Scanner(System.in);
    
    int courseNum; //course number
    String dept = ""; //course department
    int mtg;  //number of meeting tmes per week
    int time; //time of day course meets
    String profName = ""; //instructor name
    int numStud; //number of students in the class 

    //COURSE NUMBER////////////////////////////////////////////////
    System.out.print("Enter a course number: ");
    while(! scan.hasNextInt() ) //while the user enters NOT AN INTEGER for the course number
    {
      System.out.print("Error, enter a valid course number: "); //prints error message 
      scan.next(); //clears the buffer of erroneous value 
    }
    courseNum = scan.nextInt(); //assigns a correct value to the variable courseNum 

    //DEPARTMENT NAME////////////////////////////////////////////////
    System.out.print("Enter the department name: "); 
    dept = scan.nextLine(); //allows for user to enter a name with spaces, reads until the end of line

    scan.nextLine(); //clears the buffer

    //NUMBER OF MEETINGS PER WEEK////////////////////////////////////////////////
    System.out.print("Enter number of meeting times per week: ");
    while(! scan.hasNextInt() ) //while the user enters NOT AN INTEGER for the meeting times
    {
      System.out.print("Error, enter a valid number of meeting times: "); //prints error message 
      scan.next(); //clears the buffer of erroneous value 
    }
    mtg = scan.nextInt(); //assigns a correct value to the variable courseNum 

    //MEETING TIME////////////////////////////////////////////////
    System.out.print("Enter meeting time HHMM (24 hour clock) : ");
    while(! scan.hasNextInt() ) //while the user enters NOT AN INTEGER for the meeting time
    {
      System.out.print("Error, enter a valid time in the form HHMM: "); //prints error message 
      scan.next(); //clears the buffer of erroneous value 
    }
    time = scan.nextInt(); //assigns a correct value to the variable courseNum 

    //INSTRUCTOR NAME////////////////////////////////////////////////
    System.out.print("Enter the instructor name:  "); 
    profName = scan.nextLine(); //allows for user to enter a name with spaces, reads until the end of line

    scan.nextLine(); //clears the buffer 

    //NUM STUDENTS////////////////////////////////////////////////
    System.out.print("Enter number of students: ");
    while(! scan.hasNextInt() ) //while the user enters NOT AN INTEGER for the number of students
    {
      System.out.print("Error, enter a number of students: "); //prints error message 
      scan.next(); //clears the buffer of erroneous value 
    }
    numStud = scan.nextInt(); //assigns a correct value to the variable courseNum 


    System.out.println("Course Number: " + courseNum); 
    System.out.println("Department: " + dept); 
    System.out.println("Times Per Week: " + mtg);
    System.out.println("Meeting Time: " + time); 
    System.out.println("Number of Students: " + numStud); 

    
   
  }
}

    
  
  



