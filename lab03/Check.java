//SHANNON TAYLOR/////////////
//LAB 03
//CSE 002-210
//PROGRAM DESCRIPTION: this program will gather a number of guests and a bill amount. It will calculate tip and divide amongst guests. 

import java.util.Scanner;

public class Check
{
  
  public static void main(String args[])
  {
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx: ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //converts the percentage value entered into a decimal value for computation
    System.out.print("Enter the number of people that went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost; //total cost INCLUDING TIP
    double costPerPerson; //the cost that each person will pay in total 
    int dollars ,  //whole dollar amounts
        dimes , pennies; //storing the digits to the right of the decimal point in the cost 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; 
    //get whole amount in dollars by using (int)
    dollars = (int)costPerPerson; //truncation of double makes this the whole number dollar amount each will pay 
    //get dimes amount 
    dimes = (int)(costPerPerson * 10) % 10; 
    pennies = (int)(costPerPerson * 100) % 10; //uses modulus to obtain remainder and store only the dimes/pennies amount respectively
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes +  pennies);
    

  }
  
}


