/**
 * @author Shannon Taylor
 * homework 9 program 1
 * This program takes in 15 integer grades from 0 to 100 and verifies the input.
 * It prompts user for a number to search for, and performs a binary search, giving
 * the number of iterations to find that number. It then scrambles the list,
 * re prompts the user, and then performs a linear search, giving number of iterations
 * to find that number. if not found in either case, a message prints. 
 */

import java.util.Scanner;
import java.util.Random; 

public class CSE2Linear {

    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        int [] grades = new int[15]; //array of 15 grades stored
        int check, search;
        
        System.out.println("Enter 15 ascending int for final grades in CSE2");
        
        for(int i = 0; i < grades.length; i++) {
            System.out.print("Enter grade #" + (i+1) + ":");
           
            //check if an integer
            while(! scan.hasNextInt()){ 
                System.out.println("Not an integer.");
                System.out.print("Enter grade #" + (i+1) + ":");
                scan.next(); //clears buffer of erraneous value  
            }
            check = scan.nextInt(); //temporary value, not in array yet
            //check if between 0 and 100
            while(check < 0 || check > 100){ 
                System.out.println("Not in range 0 to 100.");
                System.out.print("Enter grade #" + (i+1) + ":");
                check = scan.nextInt();
            }
            //check if this value is greater than the last 
            if(i > 0) { //first index automatically ok 
                while(grades[i-1] > check) {
                    System.out.println("Must be greater than/equal to " + 
                            grades[i-1]);
                    System.out.print("Enter grade #" + (i+1) + ":");
                    check = scan.nextInt();   
                }   
            }
            grades[i] = check; 
        }
        
        //prints out the array of grades 
        printIt(grades);
        
        //prompt user for grade to be searched for (using binary search)
        System.out.print("Enter a value to search for: ");
         
        while(! scan.hasNextInt()){ 
                System.out.println("Not an integer.");
                System.out.print("Enter a value to search for: " );
                scan.next(); //clears buffer of erraneous value  
            }
        search = scan.nextInt(); 
        
        binarySearch(grades, search);

        scramble(rand,grades);
        printIt(grades);

        //prompt user for grade to be searched for (using linear search)
        System.out.print("Enter a value to search for: ");
        while(! scan.hasNextInt()){ 
                System.out.println("Not an integer.");
                System.out.print("Enter a value to search for: " );
                scan.next(); //clears buffer of erraneous value  
            }
        search = scan.nextInt(); 
        
        linearSearch(grades, search); 
        
    }
    ////////////////////////////////////////////////////////////////////////////
    //counts number of iterations through the loop, prints out 
    public static void binarySearch(int[] list, int findIt) {
        System.out.println("Binary searching...");
        int mid; //i is not in loop scope, can be used to determine num iterations
        int low = 0, hi = list.length - 1, i = 0; 
        //System.out.println("Binary searching...");
        while(hi >= low) {
            mid = (hi + low) / 2; 
            if(list[mid] == findIt) {
                System.out.println(findIt + " was found in the list with " +
                        i + " iteration(s)");
                return;   
            }
            else if(list[mid] > findIt) {
                hi = mid + 1; //go to top half of array 
            }
            else if(list[mid] < findIt) {
                low = mid - 1; //go to bottom half of array 
            }
            i++;
        }
        //only reaches this statement if not found in list
        System.out.println(findIt + " was NOT found in the list with " +
                i + " iterations");       
    }

    ////////////////////////////////////////////////////////////////////////////
    //counts number of iterations through the loop, prints out 
    public static void linearSearch(int[] list, int findIt) {
        System.out.println("Linear searching...");
        boolean found = false;
        int i; //i is not in loop scope, can be used to determine number of iterations
        for(i = 0; i < list.length; i++) {
            if(list[i] == findIt) { //if it is found, it exits loop immediately
                found = true; 
                break; 
            }   
        }
        if(found == true) {
            System.out.println(findIt + " was found in the list with " +
                        i + " iteration(s)");  
        }
        else {
            System.out.println(findIt + " was NOT found in the list with " +
                        i + " iterations");    
        }

    }
    ////////////////////////////////////////////////////////////////////////////
    //returns int array that is scrambled
    public static int[] scramble(Random rand, int[] list) {
        final int SWAP_AMT = 100; 
        int randi, temp; 
        System.out.println("Scrambled:");
        
        for(int i = 0; i <= SWAP_AMT; i++) {
            randi = rand.nextInt(15); //random num btwn 0 and 14 (indices)
            temp = list[0];
            list[0] = list[randi];
            list[randi] = temp;  
        }
        
        return list;
    }
    ////////////////////////////////////////////////////////////////////////////
    //prints whatever array you tell it to 
    public static void printIt(int[] list) {
        for(int val : list) 
            System.out.print(val + " ");
        System.out.println();
        
    }
    ////////////////////////////////////////////////////////////////////////////
    
    
    
}
