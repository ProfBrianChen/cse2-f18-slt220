/**
 * @author Shannon
 * homework 9 program 2
 * This program creates integer arrays filled with random numbers. it allows the
 * user to delete specific indices of that array, or specific values from that
 * array. method descriptions provide more insight. Main method was provided.
 */

import java.util.Scanner; 
public class RemoveElements {

    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
        String answer="";
        
        do{
            System.out.print("Random input 10 ints [0-9] ");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);
            
            
   
         
            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
            System.out.println(out1);
        
            
            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
        
        }while(answer.equals("Y") || answer.equals("y"));
        
        
    }

    ////////////////////////////////////////////////////////////////////////////
    public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
    }
    ////////////////////////////////////////////////////////////////////////////
    /*
        randomInput returns an integer array of size 10 filled with ints from
        0 to 9. It takes no parameters. 
    */
    public static int[] randomInput() {
        int [] list = new int[10]; //integer array of 10 elements
        int rand;
        
        for(int i = 0; i < list.length; i++) {
            rand = (int)(Math.random()*10); //generates random num 0 to 9 
            list[i] = rand; //sets element at index i equal to random num
        }
        
        return list;   
    }
    ////////////////////////////////////////////////////////////////////////////
      /*
        delete takes an int array and int pos as parameters. it checks if pos is
        a valid index (0-9). if not, an error is printed. if valid, it deletes
        the element at that index pos.
        it returns a correspinding int array.
    */
    public static int[] delete(int[] list, int pos) {
        if(pos < 0 || pos > 9) {
            System.out.println("The index was not valid");
            return list; 
        }
        else { //create array of 1 element fewer than list
            System.out.println("Index " + pos + " has been removed");
            int [] newList = new int[list.length - 1]; //1 element fewer than list

            //i is index of newList, j is index of list
            for(int i = 0, j=0; i < newList.length; i++,j++) {
                if(i == pos) 
                    ++j; //bumps the index of list forward, prevents out of bounds

                newList[i] = list[j]; 
            }
            return newList;
        
        }
    }
    ////////////////////////////////////////////////////////////////////////////
      /*
        remove takes an int array and int target value as parameters. it uses a
        linear search to determine if target is in the array. if not, it prints
        a corresponding message. if in the array, it creates a new array with 
        all instances of element target removed. it returns an int array 
    */
    public static int[] remove(int[] list, int target) {
        //search the array for number of instances of target
        //LINEAR SEARCH (because the elements are random, not ordered)
        //small enough arrays that search time difference will be negligable
        int count = 0;
        for(int i = 0; i < list.length; i++) {
            if(list[i] == target) //if element at index == target val
                ++count; //increment the counter 
        }
        
        if(count == 0) { //if target was never found
            System.out.println("Element " + target + " was never found");
            return list; 
            
        }
        else { //create new array only holding the elements leftover
            System.out.println("Element " + target + " has been found");
            int [] newList = new int[list.length - count]; 

            //fills new array only with acceptable elements (not target) 
            for(int i = 0, j = 0; i < newList.length; i++,j++) {
                while(list[j] == target) { 
                    ++j; 
                }
                newList[i] = list[j]; 
            }
            return newList; 
        }
    }
    
}
    
    
