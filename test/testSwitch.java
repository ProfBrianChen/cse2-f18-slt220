import java.util.Scanner; //imports the scanner class 

public class testSwitch //class name 
{
 
  public static void main(String args[]) //main method defined within here 
  {
    Scanner myScan = new Scanner(System.in); //creates a new object of the scanner class called  "myScan" 
    //Variable declarations
    int x, y, z; //the three variables that the user will enter
    int max, mid, min; //sorting variables that will store the maximum, middle, and minimum values 
    
    //gets user input of x, y, z
    System.out.print("Enter an integer x ");
    x = myScan.nextInt(); //calls nextInt method, stores the next variable that user enters as x 
    System.out.print("Enter an integer y ");
    y = myScan.nextInt(); //calls nextInt method, stores the next variable that user enters as y
    System.out.print("Enter an integer z ");
    z = myScan.nextInt(); //calls nextInt method, stores the next variable that user enters as z 
    
    //we know ONE of the three variables must be the maximum value, so we will have 3 big statements to check such
    //nested if statements within these will check if the remaining two variables are larger/smaller and assign accordingly 
    
    //checks if x could be our max
    if(x >y && x >z) //checks if x bigger than y AND x bigger than z 
    {
      max = x; 
      if(y>z) //checks relationship of two remaining variables (y,z)
      {
        mid = y;
        min = z;
      }
      else //if y isn't bigger than z, it must be the smallest (since we are nested)
      {
        mid = z; 
        min = y;
      }
      System.out.println("Maximum: " + max + "\n" + "Middle: " + mid + "\n" + "Minimum: " + min);
    }
    
    //reaches here if x was not max. now checks if y  could be our max
    else if(y >x && y >z) //checks if y bigger than x AND y bigger than z 
    {
      max = y; 
      if(x>z) //checks relationship of two remaining variables (x, z)
      {
        mid = x;
        min = z;
      }
      else //if x isn't bigger than z, it must be the smallest (since we are nested)
      {
        mid = z; 
        min = x;
      }
      System.out.println("Maximum: " + max + "\n" + "Middle: " + mid + "\n" + "Minimum: " + min);
    }
    
    //reaches here if x or y were not max . now checks if y  could be our max
    else if(z>x && z>y) //checks if z bigger than x AND z bigger than y
    {
      max = z; 
      if(x>y) //checks relationship of two remaining variables (x, y)
      {
        mid = x;
        min = y;
      }
      else //if x isn't bigger than y, it must be the smallest (since we are nested)
      {
        mid = y; 
        min = x;
      }
      System.out.println("Maximum: " + max + "\n" + "Middle: " + mid + "\n" + "Minimum: " + min);
    }
    
    //if one or more of the values are equal we have an error! 
    else
    {
      System.out.println("All three values entered were equal, and cannot be sorted. Try again.");
    }
  
  } //closes main method
} //closes class


  
 