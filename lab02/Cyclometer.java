//SHANNON TAYLOR/////////////
//LAB 02
//CSE 002-210
//PROGRAM DESCRIPTION:

public class Cyclometer
{
  
  public static void main(String args[])
  {
    //MAIN DECLARED VARIABLES
    int secsTrip1 = 480; //duration of Trip 1 IN SECONDS
    int secsTrip2 = 3220; //duration of Trip 2 IN SECONDS
    int countsTrip1 = 1561; //rotations of front wheel in Trip 1
    int countsTrip2 = 9037; //rotations of front wheel in Trip 2 
    //INTERMEDIATE VARIABLES (for calculations)
    double wheelDiameter = 27.0; //diameter of bike wheel 
    final double PI = 3.14159; 
    double feetPerMile = 5280;
    double inchesPerFoot = 12;
    double secondsPerMinute = 60; 
    double distanceTrip1; //total distance of Trip 1 IN MILES
    double distanceTrip2; //distance of Trip 2 IN MILES
    double totalDistance; //total distance (sum) of both trips IN MILES 
    
    //INITIAL PRINTS, TIME IN MINUTES AND NUMBER OF COUNTS
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes " +
                      "and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes " +
                      "and had " + countsTrip2 + " counts.");
    //CALCULATIONS FOR DISTANCES IN MILES
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //gives distance of Trip 1 in INCHES (circumfrance * counts = dist)  
    distanceTrip1 /= inchesPerFoot * feetPerMile; //converts inches to MILES, REASSIGNS
    
    distanceTrip2 = countsTrip2 * wheelDiameter * PI; //gives distance of Trip 1 in INCHES (circumfrance * counts = dist)  
    distanceTrip2 /= inchesPerFoot * feetPerMile; //converts inches to MILES, REASSIGNS
    
    //TOTAL TRIP DISTANCE
    totalDistance = distanceTrip1 + distanceTrip2;
    
    //PRINT THE DISTANCES OF TRIPS, TOTAL TRIP 
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles");
    
  }
  
}

    